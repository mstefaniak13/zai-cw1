<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//

// Route::post('reserve',function (){
// });

Route::group([
    'namespace' => 'API'
], function() {
    Route::get('viewings','ViewingController@index');
    Route::get('myviewings','ViewingController@indexauth');
    Route::get('viewing/{id}','ViewingController@viewing');
    Route::post('reserve','ViewingController@reserve');
});

Route::group([
    'prefix' => 'auth',
    'namespace' => 'API',
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

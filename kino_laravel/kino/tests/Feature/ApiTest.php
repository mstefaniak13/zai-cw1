<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Models\Movie;
use App\Models\Hall;
use App\Models\Viewing;
use App\Models\User;
use App\Models\Reservation;
use Laravel\Passport\Passport;

class ApiTests extends TestCase
{
        use DatabaseTransactions;


    use DatabaseTransactions;
    
    public function testViewings()
    {
        for($i=0; $i<= 3; ++$i) {
            $movie = factory(Movie::class)->create();
            $hall =  factory(Hall::class)->create();
            $viewing = factory(Viewing::class)->create(['hall_id'=>$hall->id, 'movie_id'=>$movie->id]);
        }
        $response = $this->getJson('/api/viewings');
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'name',
                    'description',
                    'viewings' => [
                        '*' => [
                            'id',
                            'data',
                            'date',
                            'hallName'
                        ]
                    ]
                ]
            ]);
    }

    public function testSingleViewing()
    {
        $movie = factory(Movie::class)->create();
        $hall =  factory(Hall::class)->create();
        $viewing = factory(Viewing::class)->create(['hall_id'=>$hall->id, 'movie_id'=>$movie->id]);
        
        $response = $this->getJson('/api/viewing/' . $viewing->id);
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'name',
                    'description',
                    'hall',
                    'data',
                    'seatings' => [
                        '*' => [
                            '*'
                        ]
                    ]
                ]
            ]);
    }


    public function testAuth() {
        $user = factory(User::class)->create();
        $movie = factory(Movie::class)->create();
        $hall =  factory(Hall::class)->create();
        $viewing = factory(Viewing::class)->create(['hall_id'=>$hall->id, 'movie_id'=>$movie->id, 'date'=>'2077-01-01']);
        $reservation = factory(Reservation::class)->create(['viewing_id'=>$viewing->id, 'user_id'=>$user->id]);
        Passport::actingAs(
            $user
        );
        $response = $this->getJson('/api/myviewings')
            ->assertStatus(200)
            ->assertExactJson([
                '0' => [
                    'data' => date("H:i d.m.Y", strtotime($viewing->date)),
                    'name' => $movie->name,
                    'description' => $movie->description,
                    'hallName' => $hall->name,
                    'seats' => [

                    ],
                ]
            ]);
    }
}

<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;

#models
use App\Models\Reservation;
use App\Models\Viewing;



class ClearViewings extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viewings:clear-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Czyszczenie przestarzałych seansow';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = date('Y-m-d H:i:s');
        Viewing::where('status',1)->where('date','<=',$now)->update(['status',0]);
    }

}
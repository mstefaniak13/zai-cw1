<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Viewing;
use App\Models\Movie;
use App\Models\Seat;
use App\Models\Reservation;

class ViewingController extends Controller {
    
    public function __construct() {
        $this->dateFormat = "H:i d.m.Y";
    }


    public function index() {
        $now = date('Y-m-d H:i:s');
        $viewings = Viewing::where('status',1)->where('date','>=', $now);
        $movies = Movie::whereIn('id',$viewings->pluck('movie_id'))->select('id','name','description')->get();
        $viewings = $viewings->select("id","hall_id","movie_id","date")->with("hall:id,name")->get();
        foreach($movies as $movie) {
            $data = array_values($viewings->where('movie_id',$movie->id)->map(function($x) {
                return [
                    'id' => $x->id,
                    'date' => $x->date,
                    'data' => date($this->dateFormat, strtotime($x->date)),
                    'hallName' => $x->hall->name
                ];
            })->toArray());
            usort($data, function($a,$b) {
                return ($a['date'] > $b['date']) ? 1 : -1;
            });
            $movie->viewings = $data;
        }
        return response()->json($movies);
    }

    public function indexauth() {
        if(is_null(auth('api')->user()))
            return response()->json(null);
        $now = date('Y-m-d H:i:s');
        $user = auth('api')->user()->id;
        $reservations = Reservation::where('status',1)->where('user_id',$user)->whereHas('viewing', function($q) use ($now){
            $q->where('status',1)->where('date','>=', $now);
        })->get();
        $viewings = Viewing::whereIn('id',$reservations->pluck('viewing_id'));
        $movies = Movie::whereIn('id',$viewings->pluck('movie_id'))->select('id','name','description')->get();
        $viewings = $viewings->select("id","hall_id","movie_id","date")->with("hall:id,name")->get();
        $result = [];
        foreach($reservations as $reservation) {
            $result[] = [
                'description' => $reservation->viewing->movie->description,
                'name' => $reservation->viewing->movie->name,
                'data' => date($this->dateFormat, strtotime($reservation->viewing->date)),
                'hallName' => $reservation->viewing->hall->name,
                'seats' => $reservation->seats()->get(),
            ];
        }
        return response()->json($result);
    }

    public function viewing(Request $request) {
        $viewing = Viewing::with('movie')->findOrFail($request->id);
        $seats = Seat::where('hall_id',$viewing->hall_id)->get();
        $reservations = Reservation::where('viewing_id',$viewing->id)->with('seats')->get()
            ->map(function($reservation) {
                return $reservation->seats->pluck('id');
            })->toArray();
        if(is_array($reservations) && isset($reservations[0]) && is_array($reservations[0]))
            $reservations = array_merge(...$reservations);
        $seating = [];
        foreach($seats as $seat) {
            $seating[$seat->column][$seat->row] = in_array($seat->id,$reservations);
        }
        $movie = [[
            'id' => $viewing->id,
            'name' => $viewing->movie->name,
            'description' => $viewing->movie->description,
            'data' =>  date($this->dateFormat, strtotime($viewing->date)),
            'hall' => $viewing->hall_id,
            'seatings' => $seating
        ]];

        return response()->json($movie);
    }

    public function reserve(Request $request) {
        $viewing = Viewing::find($request->idViewing);
        if(!isset($viewing->id)) {
            return response()->json(array(
                "message"=>"The given data was invalid",
                "success"=>false,
                "errors"=>[
                    "viewing" => ["No viewing found."]
                ]
            ));
        }
        //check if you can reserve that place
        $seatIds = [];
        foreach ($request->seats as $seat) {
            $aSeat = Seat::where('column',$seat['column'])->where('row',$seat['row'])->where('hall_id',$viewing->hall_id)->first();
            if(!isset($aSeat->id)) {
                return response()->json(array(
                    "message"=>"The given data was invalid",
                    "success"=>false,
                    "errors"=>[
                        "seats" => ["Invalid seat."]
                    ]
                ));
            }
            $seatIds[] = $aSeat->id;
        }

        $taken = false;
        foreach($viewing->reservations() as $reservation) {
            $checkSeats = $reservation->whereHas(function($q) use ($seatIds) {
                $q->whereIn('id',$seatIds);
            })->toArray();
            if (!empty($checkSeats)) {
                $taken = true;
                break;
            }
        }
        if($taken) {
            return response()->json(array(
                "message"=>"The given data was invalid",
                "success"=>false,
                "errors"=>[
                    "seats" => ["Seat was already taken."]
                ]
            ));
        }

        //check if user need to register
        if(isset(auth('api')->user()->id)) {
            $request->user = auth('api')->user()->id;
        } else {
            $request->validate( [
                'name'     => 'required|string',
                'email'    => 'required|string|email|unique:users',
                'password' => 'required|string|confirmed'
            ] );
            $user = new User( [
                'name'     => $request->name,
                'email'    => $request->email,
                'password' => Hash::make( $request->password ),
                'accepts_spam' => $request->accepts_spam,
            ] );
            $user->save();
            $request->user = $user->id;
        }

        //create reservation
        $reservation = new Reservation( [
            'viewing_id' => $viewing->id,
            'user_id' => $request->user,
            'status' => 1
        ]);
        $reservation->save();
        $reservation->seats()->syncWithoutDetaching($seatIds);
        return response()->json(array(
            "success" => true,
            "message" => "Successfully created reservation!"
        ));
    }
}

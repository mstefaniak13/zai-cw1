<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Viewing extends Model
{
    
    protected $fillable = [
        'date','hall_id','movie_id','status'
    ];

    protected $casts = [
        'date' => 'datetime',
    ];

    public function hall() {
        return $this->belongsTo('App\Models\Hall','hall_id','id');
    }
    
    public function movie() {
        return $this->belongsTo('App\Models\Movie','movie_id','id');
    }

    public function reservations() {
        return $this->hasMany('App\Models\Reservation','viewing_id','id');
    }
}

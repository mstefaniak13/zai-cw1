<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $fillable = [
        'name', 'description'
    ];

    public function viewings() {
        return $this->hasMany('App\Models\Viewing', 'movie_id', 'id');
    }
}

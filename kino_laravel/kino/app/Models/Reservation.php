<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
        'status','viewing_id','user_id'
    ];

    public function user() {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function viewing() {
        return $this->belongsTo('App\Models\Viewing','viewing_id','id');
    }
    
    public function seats() {
        return $this->belongsToMany('App\Models\Seat','reservations_seats','reservation_id','seat_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hall extends Model
{
    protected $fillable = [
        'name'
    ];

    public function seats() {
        return $this->hasMany('App\Models\Seat', 'hall_id', 'id');
    }
}

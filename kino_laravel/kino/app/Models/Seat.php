<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seat extends Model
{
    protected $fillable = [
        'row', 'column','hall_id'
    ];

    public function hall() {
        return $this->belongsTo('App\Models\Hall','hall_id','id');
    }
    
    public function reservations() {
        return $this->belongsToMany('App\Models\Reservation','reservations_seats','seat_id','reservation_id');
    }
}

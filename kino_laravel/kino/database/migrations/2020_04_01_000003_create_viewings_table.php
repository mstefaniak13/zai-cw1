<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewingsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'viewings';

    /**
     * Run the migrations.
     * @table viewings
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('movie_id');
            $table->unsignedInteger('hall_id');
            $table->integer('status');
            $table->dateTime('date');
            $table->timestamps();

            $table->index(["hall_id"], 'fk_halls_idx');

            $table->index(["movie_id"], 'fk_movies_idx');
            

            $table->foreign('movie_id', 'fk_movies_idx')
                ->references('id')->on('movies')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('hall_id', 'fk_halls_idx')
                ->references('id')->on('halls')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

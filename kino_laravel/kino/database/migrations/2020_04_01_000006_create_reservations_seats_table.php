<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsSeatsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'reservations_seats';

    /**
     * Run the migrations.
     * @table reservations_seats
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('reservation_id');
            $table->unsignedInteger('seat_id');
            $table->primary(array('reservation_id','seat_id'));
            $table->index(["reservation_id"], 'fk_reservation_seats_reservations_idx');

            $table->index(["seat_id"], 'fk_reservation_seat_seats_idx');


            $table->foreign('reservation_id', 'fk_reservation_seats_reservations_idx')
                ->references('id')->on('reservations')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('seat_id', 'fk_reservation_seat_seats_idx')
                ->references('id')->on('seats')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

testString = "Ksylofon"
data = {'first': 'Hodor', 'last': 'Hodor!'}
data2 = {'first': 'Hodor!', 'last': 'Hodor'}
person = {'first': 'Jean-Luc', 'last': 'Picard'}

print('{:10.5}'.format(testString))

print('{:07d}'.format(410))

print('{first} {last}'.format(**data))
print('{first} {last}'.format(**data2))

print('{p[first]} {p[last]}'.format(p=person))

print('{:{align}{width}}'.format('test', align='^', width='10'))

text = "Lorem Ipsum jest tekstem stosowanym jako przykładowy wypełniacz \
w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. \
przez nieznanego drukarza do wypełnienia tekstem próbnej książki. \
Pięć wieków później zaczął być używany przemyśle elektronicznym, \
pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. \
XX w. wraz z publikacją arkuszy Letrasetu, zawierających fragmenty \
Lorem Ipsum, a ostatnio z zawierającym różne wersje Lorem Ipsum \
oprogramowaniem przeznaczonym do realizacji druków na komputerach \
osobistych, jak Aldus PageMaker"

name = "Marcin"
surname = "Stefaniak"
letter_1 = name[2]
letter_2 = surname[3]
print("W tekście jest %d liter %s oraz %d liter %s"%(text.count(letter_1), letter_1, text.count(letter_2), letter_2))

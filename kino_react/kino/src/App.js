import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";


import './App.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import FrontPage from "./pages/FrontPage"
import Header from "./components/Header"
import Footer from "./components/Footer"
import Page404 from "./components/404Page"
import ListingMovies from "./pages/ListingMovies"
import LoginPageContent from "./components/LoginPage"
import RegisterPage from "./components/RegisterPage"
import LogoutPage from "./pages/LogoutPage"
import MyMovies from "./pages/MyMovies"
import SingleMovie from "./pages/singleMovie"

class App extends React.Component{

    constructor(props) {
        super(props);
        this.state =
            {
                active: false,
                token: ''
            };
        const token = window.localStorage.getItem('token')
        if(token !== null)
        {
            //TODO check if token is valid
            this.state = {
                active: true,
                token: token
            }
        }
    }

    updateActiveState(active, token){
        window.localStorage.setItem('token', token)
        this.setState({active: active, token: token})
    }

    render(){

        return (
            <div className="App">
                <Router>
                    <Header dataLogin={this.state.active}/>
                    <Switch>

                        <Route path="/logowanie">
                            <LoginPageContent dataLoginActive={this.state.active}  dataLogin={this.updateActiveState.bind(this)} />
                        </Route>
                        <Route path="/rejestracja">
                            <RegisterPage/>
                        </Route>
                        <Route path="/seans/:id" render={(props) => <SingleMovie {...props} active={this.state.active} />} />
                        <Route path="/seanse">
                            <ListingMovies/>
                        </Route>
                        <Route path="/moje-seanse">
                            <MyMovies active={this.state.active} />
                        </Route>
                        <Route path="/wylogowanie">
                            <LogoutPage token={this.state.token} dataLogin={this.updateActiveState.bind(this)} />
                        </Route>
                        <Route exact path="/">
                            <FrontPage/>
                        </Route>
                        <Route path="*">
                            <Page404/>
                        </Route>
                    </Switch>
                    <Footer/>
                </Router>
            </div>
        );
    }

}

export default App;

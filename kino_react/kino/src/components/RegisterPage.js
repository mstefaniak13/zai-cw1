import React from 'react';
import ApiRoutes from "../config"
const axios = require('axios')

class RegisterPage extends React.Component{

    constructor(props) {
        super(props);
        this.state = { name: '', email: '', password: '', password2: '', alertText: '', isSending: false};
    }

    change(e){
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    render() {
        return (
            <div className="main">
                <div className={'info'}>
                    <form className={"form" + (this.state.isSending ? ' -sending' : '' )}>
                        <h2>Rejestracja</h2>
                        <div className={'form__alert'}>{this.state.alertText}</div>
                        <div className={"form__input"}>
                            <label>Imię i nazwisko</label>
                            <input value={this.state.name} onChange={e => this.change(e)} type="text" name="name"/>
                        </div>
                        <div className={"form__input"}>
                            <label>Email</label>
                            <input value={this.state.email} onChange={e => this.change(e)} type="email" name="email"/>
                        </div>
                        <div className={"form__input"}>
                            <label>Hasło</label>
                            <input value={this.state.password} type="password" onChange={e => this.change(e)} name="password"/>
                        </div>
                        <div className={"form__input"}>
                            <label>Powtórz hasło</label>
                            <input value={this.state.password2} type="password" onChange={e => this.change(e)} name="password2"/>
                        </div>
                        <button onClick={this.action.bind(this)}>Wyślij</button>
                    </form>
                </div>
            </div>
        );
    }

    action(e){
        e.preventDefault()
        this.setState({isSending: true})
        axios.post(ApiRoutes.RegisterRoute, {
            name: this.state.name, email: this.state.email, password: this.state.password, password_confirmation: this.state.password2, accepts_spam: 1
        }
        ).then(function (response) {
            this.setState({alertText: response.data.message ,isSending: false})
        }.bind(this)).catch(function (err) {

            let errorText = '';
            for (let key in  err.response.data.errors) {
                if (err.response.data.errors.hasOwnProperty(key))
                {
                    errorText += ("\n" + err.response.data.errors[key].join("\n"))
                }
            }
            this.setState({alertText: errorText ,isSending: false})
        }.bind(this))

    }
}

export default RegisterPage;

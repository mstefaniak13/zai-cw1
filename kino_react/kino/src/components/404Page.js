import React from 'react';

function Page404() {
    return (
        <div className={'info'}>
            <div className="main">
                <div className={'page404'}>
                    <p >Nastąpił błąd: <strong> 404 - strona o podanym adresie nie istnieje. </strong>	</p>
                    <div>Spróbuj wykonać następujące operacje:</div>
                    <ul>
                        <li>• Spróbuj otworzyć <a href="/">stronę główną</a> serwisu</li>
                        <li>• Spróbuj połączyć się ze stroną z innego komputera</li>
                        <li>• Jeżeli problem pozostanie możesz skontaktować się z właścicielem strony</li>
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default Page404;

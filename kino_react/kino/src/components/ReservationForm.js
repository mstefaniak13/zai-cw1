import React from "react"
import ApiRoutes from "../config"
import {Redirect} from 'react-router-dom'
const axios = require('axios')

class ReservationForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            isSending: false,
            alertText: '',
            email: '',
            name: '',
            password: '',
            password2: '',
            redirect: false
        }
    }

    change(e){
        this.setState({
            [e.target.name] : e.target.value
        })
    }


    render() {
        const { redirect } = this.state;

        if (redirect)
            return <Redirect to='/moje-seanse'/>;
        return (
            <div className="main listing-movies">
                <div>
                    <h1 style={{marginBottom: 20 +'px'}}>Zarezerwuj seans</h1>
                    <div style={{cursor: 'pointer'}} onClick={this.props.seatClear}>{'<-'}Wróć do wyboru miejsca</div><br/><br/><br/>
                    <form className={"form" + (this.state.isSending ? ' -sending' : '' )}>
                        <div className={'form__alert'}>{this.state.alertText}</div>
                        <div>Data: {this.props.viewing.data}</div>
                        <div>{this.props.viewing.name}</div>
                        {
                            this.props.seats.map((obj) => (<div key={obj.row + '-' + obj.column}>Miejsce: {obj.row} rząd, miejsce {obj.column}</div>))
                        }
                        <div style={{ display :  this.props.active ? 'none' : ''}}>
                            <div  style={{marginTop: 10 +'px'}} className={"form__input"}>
                                <label>Imię i nazwisko</label>
                                <input value={this.state.name} onChange={e => this.change(e)} type="text" name="name"/>
                            </div>
                            <div className={"form__input"}>
                                <label>Email</label>
                                <input value={this.state.email} onChange={e => this.change(e)} type="email" name="email"/>
                            </div>
                            <div className={"form__input"}>
                                <label>Hasło</label>
                                <input value={this.state.password} type="password" onChange={e => this.change(e)} name="password"/>
                            </div>
                            <div className={"form__input"}>
                                <label>Powtórz hasło</label>
                                <input value={this.state.password2} type="password" onChange={e => this.change(e)} name="password2"/>
                            </div>
                        </div>
                        <button style={{marginTop: 10 +'px'}} onClick={this.action.bind(this)}>Zarezerwuj</button>
                    </form>
                </div>
            </div>)
    }
    action(e){
        e.preventDefault()
        console.log(this.state);
        this.setState({isSending: true})
        axios.post(ApiRoutes.reservationRoute, {
                name: this.state.name, email: this.state.email, password: this.state.password, password_confirmation: this.state.password2, accepts_spam: 1,
            date: this.props.viewing.data, nameViewing: this.props.viewing.name, seats: this.props.seats, idViewing:  this.props.viewing.id
            },
            {
            headers : {'Authorization': 'Bearer ' + window.localStorage.getItem('token')}
            }
        ).then(function (response) {
            this.setState({alertText: response.data.message ,isSending: false})
            if(typeof(response.data.success) !== 'undefined' && response.data.success) {
                this.setState({ redirect: true });
            }
        }.bind(this)).catch(function (err) {
            console.log(err);
            let errorText = '';
            for (let key in  err.response.data.errors) {
                if (err.response.data.errors.hasOwnProperty(key))
                {
                    errorText += ("\n" + err.response.data.errors[key].join("\n"))
                }
            }
            this.setState({alertText: errorText ,isSending: false})
        }.bind(this))


    }
}

export default  ReservationForm
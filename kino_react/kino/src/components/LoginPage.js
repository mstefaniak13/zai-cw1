import React from 'react';
import ApiRoutes from "../config"
import {Redirect} from 'react-router-dom'
const axios = require('axios')


class LoginPageContent extends React.Component{

    constructor(props) {
        super(props);
        this.state = { login: '', password: '', isSending: false, alertText: '',};
    }

    showBox(){
        this.setState({IsFormOpen: false})
    }

    change(e){
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    render() {

        return this.props.dataLoginActive ? (
                <Redirect to="/moje-seanse" />
            )
            :
            (
            <div className="main">
                <div className={'info'}>
                    <form className={"form" + (this.state.isSending ? ' -sending' : '' )}>
                        <h2>Logowanie</h2>
                        <div className={'form__alert'}>{this.state.alertText}</div>
                        <div className={"form__input"}>
                            <label>Login</label>
                            <input value={this.state.name} onChange={e => this.change(e)} type="email" name="login"/>
                        </div>
                        <div className={"form__input"}>
                            <label>Hasło</label>
                            <input value={this.state.password} type="password" onChange={e => this.change(e)} name="password"/>
                        </div>
                        <button onClick={this.loginAction.bind(this)}>Wyślij</button>
                    </form>
                </div>
            </div>
        );
    }

    loginAction(e){
        e.preventDefault()
        this.setState({isSending: true, alertText: ''})
        axios.post(ApiRoutes.loginRoute,{
            email: this.state.login,
            password: this.state.password,
            remember_me: 1,
        }).then(function (response) {
            this.setState({isSending: false})
            this.props.dataLogin(true, response.data.access_token)
        }.bind(this))
        .catch(function(){
            this.props.dataLogin(false)
            this.setState({alertText: 'Logowanie się nie powiodło'})
            //TODO error login
        }.bind(this))

    }
}

export default LoginPageContent;

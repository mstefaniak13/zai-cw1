import React from 'react';
import {Link, useRouteMatch} from "react-router-dom";

function Header(props) {
    let test1 =  useRouteMatch({
        path: '',
        exact: true
    });
    let test2 =  useRouteMatch({
        path: '/seanse',
        exact: true
    });
    let test3 =  useRouteMatch({
        path: '/logowanie',
        exact: true
    });
    let test4 =  useRouteMatch({
        path: '/rejestracja',
        exact: true
    });
    let test5 =  useRouteMatch({
        path: '/moje-seanse',
        exact: true
    });
    return props.dataLogin  ?
    (
        <header>
            <ul>
                <li className={ test1 ? '-active' : ''}><Link to={'/'}>Strona główna</Link></li>
                <li className={ test2 ? '-active' : ''}><Link to={'/seanse'}>Lista seansów</Link></li>
                <li className={ test5 ? '-active' : ''}><Link to={'/moje-seanse'}>Moje seanse</Link></li>
                <li className={ test4 ? '-active' : ''}><Link to={'/wylogowanie'}>Wyloguj się</Link></li>
            </ul>
        </header>
    )
    :
    (
       <header>
           <ul>
               <li className={ test1 ? '-active' : ''}><Link to={'/'}>Strona główna</Link></li>
               <li className={ test2 ? '-active' : ''}><Link to={'/seanse'}>Lista seansów</Link></li>
               <li className={ test3 ? '-active' : ''}><Link to={'/logowanie'}>Logowanie</Link></li>
               <li className={ test4 ? '-active' : ''}><Link to={'/rejestracja'}>Rejestracja</Link></li>
           </ul>
       </header>
    );
}

export default Header;

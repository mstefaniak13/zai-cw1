const baseRoute = 'http://kino.sf';
// const baseRoute = 'http://127.0.0.1:8000';

const ApiRoutes = {
  loginRoute : baseRoute+'/api/auth/login',
  logoutRoute : baseRoute+'/api/auth/logout',
  singleViewing : baseRoute+'/api/viewing/',
  RegisterRoute: baseRoute+'/api/auth/signup',
  viewingsRoute : baseRoute+'/api/viewings',
  userviewingsRoute : baseRoute+'/api/myviewings',
  reservationRoute : baseRoute+'/api/reserve',
};



export default ApiRoutes;
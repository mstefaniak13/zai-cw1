import React from "react"
import axios from "axios"
import ApiRoutes from "../config"
import Page404 from "../components/404Page"
import ReservationForm from "../components/ReservationForm"
import Loader from "react-loader-spinner"

class SingleMovie extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            loading : true,
            viewing : null,
            error404: false,
            pickedPlaces: [],
            didPickedPlace: false
        }
        axios.get(ApiRoutes.singleViewing + this.props.match.params.id,{

        }).then(function (response) {
            this.setState({
                loading: false,
                viewing : response.data[0],
            })
        }.bind(this)).catch(function(){
            this.setState({
                loading: false,
                error404: true
            })
        }.bind(this))

    }

    render(){
        const that = this
        if(this.state.loading)
            return (
                <Loader style={{"marginTop": "100px"}} type="Circles" color="#FFF" height={300} width={300} timeout={3000} />
            )
        else if(this.state.error404)
            return (
                <Page404/>
            )
        else if(this.state.didPickedPlace)
            return <ReservationForm active={this.props.active} viewing={this.state.viewing} seats={this.state.pickedPlaces}  seatClear={this.cleanSeat.bind(this) }/>
        else
            return (
                <div className="main">
                    <div>
                        <h1>Rezerwacja miejsca - {this.state.viewing.name} - {this.state.viewing.data} </h1>
                        <span><br></br>Miejsca</span>

                        <div className={'seatings'}>
                            <div className={'seatings__display'}>Ekran</div>
                            {Object.keys(this.state.viewing.seatings).map(function(row,index){
                                return (<div className={'seatings__row'} key={index}>{
                                    Object.keys(this.state.viewing.seatings[row]).map(function (index, value) {
                                        return (<div key={value} className={'seatings__single ' + ((this.state.viewing.seatings[row][index]) ? '-active' : '') + ' ' + ( that.isObjectInArray(row, index) ? '-selected' : '') } onClick={() => (!this.state.viewing.seatings[row][index]) ? (this.isObjectInArray(row, index) ? (function () {
                                        const currentPicked = that.state.pickedPlaces.filter(a =>  !(a.row === row && a.column === index))
                                        that.setState({pickedPlaces: currentPicked})
                                        })():  this.setState({pickedPlaces: [...this.state.pickedPlaces, {row: row, column: index}]})) : null}>{index}</div>)
                                    }.bind(this))
                                }</div>)
                            }.bind(this))}
                        </div>
                        {
                            (this.state.pickedPlaces.length > 0) ?  (<button onClick={() => this.setState({didPickedPlace: true})}>Rezerwuj te miejsca</button>): null
                        }
                    </div>
                </div>
            )

    }

    isObjectInArray(row, column){
        return this.state.pickedPlaces.reduce(function (state, obj) {
            if(obj.column === column && obj.row === row)
                state = true;
            return state;
        },false)
    }

    cleanSeat(){
        this.setState({didPickedPlace: false})
    }

}
export default SingleMovie;
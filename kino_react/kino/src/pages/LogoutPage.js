import React from 'react';
import ApiRoutes from "../config"
import {Redirect} from 'react-router-dom'
const axios = require('axios')

class LogoutPage extends React.Component{

    constructor(props) {
        super(props);

        axios.get(ApiRoutes.logoutRoute,{
            headers : {
                'Authorization': 'Bearer ' + this.props.token
            }
        })

        this.props.dataLogin(false,'')
    }

    render() {
        return (
                <Redirect to="/logowanie" />
        )
    }

}

export default LogoutPage;

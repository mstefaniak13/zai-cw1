import React from 'react';

import FrontPageContent from "../components/FrontPageContent"

function FrontPage() {
    return (
        <div className={'info'}>
          <FrontPageContent/>
        </div>
    );
}

export default FrontPage;

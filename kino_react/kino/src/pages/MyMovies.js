import React, { useState, useEffect}  from 'react';
import axios from 'axios';
import ApiRoutes from "../config"
import {Redirect} from "react-router-dom"
import Loader from "react-loader-spinner"

function MyMovies(props){
    const [data, setData] = useState({ hits: [], isLoading: true});

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios(
                ApiRoutes.userviewingsRoute,
                {
                    headers : {'Authorization': 'Bearer ' + window.localStorage.getItem('token')}
                }
            );
            const res = { hits:result.data , isLoading: false }

            setData(res);
        }

        fetchData();
    }, []);

    return props.active ?
        data.isLoading ?
        (
            <Loader style={{"marginTop": "100px"}} type="Circles" color="#FFF" height={300} width={300} timeout={3000} />
        )
            :
            Array.isArray(data.hits) ?
        (
        <div className="main listing-movies">
            <div>
                <h1 style={{marginBottom: 20 +'px'}}>Moje seanse</h1>
                <ul className={"seans"}>
                    {data.hits.map(item => (
                        <li key={item.id} className="seans__single">
                            <h1>{item.name}</h1>
                            <div  className={"seans__description"}>{item.description}</div>
                            <div  className={"seans__seatings"}>Seans: {item.data}, sala: {item.hallName}, miejsca:</div>
                            {item.seats.map(item2 => (
                                <div className={"seans__seatings__single"}>Rząd: {item2.row}, Miejsce {item2.column}</div>
                            ))}
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    ) :(
                    <div className="main listing-movies">
                        <div>
                            <h1 style={{marginBottom: 20 +'px'}}>Moje seanse</h1>
                            <ul className={"seans"}>
                               Brak rezerwacji
                            </ul>
                        </div>
                    </div>
                ) : (<Redirect to="/logowanie" />);


}

export default MyMovies;

import React, { useState, useEffect}  from 'react';
import {Link} from "react-router-dom"
import axios from 'axios';
import ApiRoutes from "../config"
import Loader from 'react-loader-spinner'

function ListingMovies(){
        const [data, setData] = useState({ hits: [], isLoading: true});

        useEffect(() => {
            const fetchData = async () => {
                const result = await axios(
                    ApiRoutes.viewingsRoute
                );
                const res = { hits:result.data , isLoading: false }

                setData(res);
        }

            fetchData();
        }, []);

    return data.isLoading ?
        (
            <Loader style={{"marginTop": "100px"}} type="Circles" color="#FFF" height={300} width={300} timeout={3000} />
        )
            :
        (
        <div className="main listing-movies">
            <ul className={"seans"}>
                {data.hits.map(item => (
                    <li key={item.id} className="seans__single">
                        <h1>{item.name}</h1>
                        <div>{item.description}</div>
                        <div className="seans__single-list">
                            <div className="seans__headline">Seanse</div>
                            <div className="seans__list_viewings">
                            {item.viewings.map(item2 => (
                                <span  key={item2.id}><Link to={"seans/" + item2.id }>{item2.data}</Link></span>
                            ))}
                            </div>
                        </div>
                    </li>
                ))}
            </ul>
        </div>

    );


}

export default ListingMovies;
